import os
import shutil
import subprocess
import time

TARGET_INJECT = "Include.lua_00004.txt"
INJECT_FOLDER = "Unity_Assets_Files\scripts32\CAB-android32\\"
DEC_TOOLS = "3rdparty\dectools\\"


def cleanup(mode):
    try:
        shutil.rmtree("Unity_Assets_Files")
    except OSError:
        pass
    if mode == "mkdir":
        os.makedirs(INJECT_FOLDER, exist_ok=True)


def injectlua():
    with open(TARGET_INJECT, "r") as f:
        f_orig = f.read()
        # Customized method to inject
        split_at = """require("Mod/Battle/Include")\n"""
        f_orig = f_orig.split(split_at)
        f_orig[0] = f_orig[0] + split_at
        # End
        f_orig = list(f_orig)

    with open("inject.lua", "r") as f:
        inject_text = f.read()
        # Customized method to inject
        inject_text=inject_text.partition("weapon_list = generate_weapon_list()\n")
        f_orig.insert(1, inject_text[0]+inject_text[1])
        f_orig.insert(3, inject_text[2])
        # End
        injected = "\n".join(f_orig)

    with open(INJECT_FOLDER + TARGET_INJECT, "w+") as f:
        f.write(injected)


def decryptAB():
    subprocess.run([DEC_TOOLS + "uabDec.exe", "scripts32enc", "scripts32"])


def repackAB():
    subprocess.run(["Azcli.exe", "--repack", "scripts32"], stdout=subprocess.DEVNULL)


if __name__ == "__main__":
    print("Azur Lane Post v5 Lua Injection")
    print("By n0k0m3\n")
    print("Clean up")
    time.sleep(0.5)
    cleanup("mkdir")
    print("Inject Lua")
    time.sleep(0.5)
    injectlua()
    print("Decrypt AssetsBundle")
    time.sleep(0.5)
    decryptAB()
    print("Repack AssetsBundle")
    repackAB()
    print("Clean up again")
    time.sleep(0.5)
    cleanup(0)

    input("Press ENTER to exit...")
